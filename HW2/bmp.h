

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

typedef signed char             int8_t;   
typedef short int               int16_t;  
typedef int                     int32_t;  
#define pixelNum 262144;
#define DCT_matrix_size 8
#define pi 3.141593
struct fileheader
{
    unsigned short int type;//2bytes
    unsigned int size;// File size in bytes,全部的檔案大小 4bytes
    unsigned int reserved1; // 保留欄位, 4byte
    unsigned int offset;// Offset to image data, 4bytes
}__attribute__((packed));
typedef struct fileheader FILEHEADER;

struct infoheader
{
    unsigned int bmpHeaderSize;
    unsigned int width;
    unsigned int height; 
    unsigned short int planes;
    unsigned short int bitPerPixel;
    unsigned int compression;
    unsigned int bmpDataSize;
    unsigned int H_resolution;
    unsigned int V_resolution;
    unsigned int used_color;
    unsigned int impot_color;
}__attribute__((packed));
typedef struct infoheader INFOHEADER;

//extern unsigned char bmpData [262144][3];

struct bmp
{
    struct fileheader FILEHEADER;
    struct infoheader INFOHEADER;
    //unsigned char palette[];
    unsigned char bmpData[] ;
}__attribute__((packed));

typedef struct bmp BMP;

struct glbmp
{
    struct fileheader FILEHEADER;
    struct infoheader INFOHEADER;
    unsigned char palette[1024];
    unsigned char* glbmpData;
}__attribute__((packed));
typedef struct glbmp GLBMP;

int test_matrix[DCT_matrix_size][DCT_matrix_size] = {{16,  11,  10,  16,  24,  40,  51,  61},
                                                {12,  12,  14,  19,  26,  58,  60,  55},
                                                {14,  13,  16,  24,  40,  57,  69,  56},
                                                {14,  17,  22,  29,  51,  87,  80,  62},
                                                {18,  22,  37,  56,  68, 109, 103,  77},
                                                {24,  35,  55,  64,  81, 104, 113,  92},
                                                {49,  64,  78,  87, 103, 121, 120, 101},
                                                {72,  92,  95,  98, 112, 100, 103,  99}} ; 
unsigned char in[8][8]=	{{144,139,149,155,153,155,155,155},
				{151,151,151,159,156,156,156,158},
				{151,156,160,162,159,151,151,151},
				{158,163,161,160,160,160,160,161},
				{158,160,161,162,160,155,155,156},
				{161,161,161,161,160,157,157,157},
				{162,162,161,160,161,157,157,157},
				{162,162,161,160,163,157,158,154}};


float out[8][8]={{1257.9,      2.3,     -9.7,     -4.1,      3.9,      0.6,     -2.1,      0.7}, 
   {-21.0  ,  -15.3 ,    -4.3 ,    -2.7  ,    2.3 ,     3.5 ,     2.1 ,    -3.1},  
   {-11.2  ,   -7.6 ,    -0.9 ,     4.1  ,    2.0 ,     3.4,      1.4 ,     0.9},  
    {-4.9   ,  -5.8  ,    1.8 ,     1.1  ,    1.6 ,     2.7 ,     2.8 ,    -0.7},  
    { 0.1   ,  -3.8 ,     0.5 ,     1.3  ,   -1.4 ,     0.7 ,     1.0 ,     0.9},  
    { 0.9  ,   -1.6 ,     0.9 ,    -0.3  ,   -1.8 ,    -0.3,      1.4 ,     0.8},  
    {-4.4  ,    2.7 ,    -4.4 ,    -1.5  ,   -0.1 ,     1.1,      0.4 ,     1.9},  
    {-6.4  ,    3.8 ,    -5.0 ,    -2.6  ,    1.6 ,     0.6 ,     0.1 ,     1.5}}; 