import cv2
import numpy as np
from sklearn.metrics import pairwise
import time
bg = None

#--------------------------------------------------
# To find the running average over the background
#--------------------------------------------------
def run_avg(image, aWeight):
    global bg
    # initialize the background
    if bg is None:
        bg = image.copy().astype("float")
        return

    # compute weighted average, accumulate it and update the background
    cv2.accumulateWeighted(image, bg, aWeight)

#--------------------------------------------------------------
# To count the number of fingers in the segmented hand region
#--------------------------------------------------------------
def count(thresholded, segmented):
    # find the convex hull of the segmented hand region
    chull = cv2.convexHull(segmented)

    # find the most extreme points in the convex hull
    extreme_top    = tuple(chull[chull[:, :, 1].argmin()][0])
    extreme_bottom = tuple(chull[chull[:, :, 1].argmax()][0])
    extreme_left   = tuple(chull[chull[:, :, 0].argmin()][0])
    extreme_right  = tuple(chull[chull[:, :, 0].argmax()][0])

    # find the center of the palm
    cX = int((extreme_left[0] + extreme_right[0]) / 2)
    cY = int((extreme_top[1] + extreme_bottom[1]) / 2)

    # find the maximum euclidean distance between the center of the palm
    # and the most extreme points of the convex hull
    distance = pairwise.euclidean_distances([(cX, cY)], Y=[extreme_left, extreme_right, extreme_top, extreme_bottom])[0]
    maximum_distance = distance[distance.argmax()]

    # calculate the radius of the circle with 80% of the max euclidean distance obtained
    radius = int(0.8 * maximum_distance)

    # find the circumference of the circle
    circumference = (2 * np.pi * radius)

    # take out the circular region of interest which has 
    # the palm and the fingers
    circular_roi = np.zeros(thresholded.shape[:2], dtype="uint8")
	
    # draw the circular ROI
    cv2.circle(circular_roi, (cX, cY), radius, 255, 1)

    # take bit-wise AND between thresholded hand using the circular ROI as the mask
    # which gives the cuts obtained using mask on the thresholded hand image
    circular_roi = cv2.bitwise_and(thresholded, thresholded, mask=circular_roi)

    # compute the contours in the circular ROI
    ( cnts, _) = cv2.findContours(circular_roi.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    # initalize the finger count
    count = 0

    # loop through the contours found
    for c in cnts:
        # compute the bounding box of the contour
        (x, y, w, h) = cv2.boundingRect(c)

        # increment the count of fingers only if -
        # 1. The contour region is not the wrist (bottom area)
        # 2. The number of points along the contour does not exceed
        #     25% of the circumference of the circular ROI
        if ((cY + (cY * 0.25)) > (y + h)) and ((circumference * 0.25) > c.shape[0]):
            count += 1

    return count

#---------------------------------------------
# To segment the region of hand in the image
#---------------------------------------------
def segment(image, threshold=25):
    # global bg
    # find the absolute difference between background and current frame
    # diff = cv2.absdiff(bg.astype("uint8"), image)

    # threshold the diff image so that we get the foreground
    # thresholded = cv2.threshold(diff, threshold, 255, cv2.THRESH_BINARY)[1]

    # get the contours in the thresholded image
    ( cnts, _) = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    # return None, if no contours detected
    if len(cnts) == 0:
        return
    else:
        # based on contour area, get the maximum contour which is the hand
        segmented = max(cnts, key=cv2.contourArea)
        # print ("there are " , str(len(segmented)) , " points in seg")
        #print("Hello World")
        return (segmented)

#--------------------------------------------------------------
# To count the number of fingers in the segmented hand region
#--------------------------------------------------------------
def count(thresholded, segmented):
    # find the convex hull of the segmented hand region
    chull = cv2.convexHull(segmented)
    print ("after convexHull, there are " , str(len(chull)) , " points")
    # find the most extreme points in the convex hull
    extreme_top    = tuple(chull[chull[:, :, 1].argmin()][0])
    extreme_bottom = tuple(chull[chull[:, :, 1].argmax()][0])
    extreme_left   = tuple(chull[chull[:, :, 0].argmin()][0])
    extreme_right  = tuple(chull[chull[:, :, 0].argmax()][0])

    # find the center of the palm
    cX = int((extreme_left[0] + extreme_right[0]) / 2)
    cY = int((extreme_top[1] + extreme_bottom[1]) / 2)

    # find the maximum euclidean distance between the center of the palm
    # and the most extreme points of the convex hull
    distance = pairwise.euclidean_distances([(cX, cY)], Y=[extreme_left, extreme_right, extreme_top, extreme_bottom])[0]
    maximum_distance = distance[distance.argmax()]

    # calculate the radius of the circle with 80% of the max euclidean distance obtained
    radius = int(0.8 * maximum_distance)

    # find the circumference of the circle
    circumference = (2 * np.pi * radius)

    # take out the circular region of interest which has 
    # the palm and the fingers
    circular_roi = np.zeros(thresholded.shape[:2], dtype="uint8")
	
    # draw the circular ROI
    cv2.circle(circular_roi, (cX, cY), radius, 255, 1)

    # take bit-wise AND between thresholded hand using the circular ROI as the mask
    # which gives the cuts obtained using mask on the thresholded hand image
    circular_roi = cv2.bitwise_and(thresholded, thresholded, mask=circular_roi)

    # compute the contours in the circular ROI
    ( cnts, _) = cv2.findContours(circular_roi.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    # initalize the finger count
    count = 0

    # loop through the contours found
    for c in cnts:
        # compute the bounding box of the contour
        (x, y, w, h) = cv2.boundingRect(c)

        # increment the count of fingers only if -
        # 1. The contour region is not the wrist (bottom area)
        # 2. The number of points along the contour does not exceed
        #     25% of the circumference of the circular ROI
        if ((cY + (cY * 0.25)) > (y + h)) and ((circumference * 0.25) > c.shape[0]):
            count += 1

    return count


if __name__ == "__main__":
    handCascade = cv2.CascadeClassifier('Hand_haar_cascade.xml')
    cap = cv2.VideoCapture(0)
    _, first_frame = cap.read()
    # first_gray = cv2.cvtColor(first_frame, cv2.COLOR_BGR2GRAY)
    # first_gray = cv2.GaussianBlur(first_gray, (5, 5), 0)
    num_frames=0
    aweight = 0.5
    print(cap.get(3))
    print(cap.get(4))
    while True:
        try:
            _, frame = cap.read()
            
            if num_frames < 60:
                # Wait for 2 second to take background
                run_avg(frame, aweight)
                bg_gray= cv2.cvtColor(bg.astype("uint8"), cv2.COLOR_BGR2GRAY)
                bg_gray = cv2.GaussianBlur(bg_gray, (3, 3), 0)
                # print(num_frames)
                
            else:
                # print("XXXXXXXXXXX")
                # cv2.imwrite("/home/pi/Desktop/bg.jpg",frame)
                frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                frame_gray = cv2.GaussianBlur(frame_gray, (3, 3), 0)
                difference_mask = cv2.absdiff(bg_gray, frame_gray)
                th = 30
                imask =  difference_mask>th

                canvas = np.zeros_like(frame_gray, np.uint8)
                canvas[imask] = frame_gray[imask]
                # 將canvas用膚色偵測
                ret, img = cv2.threshold(canvas, 1, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
                img = cv2.GaussianBlur(img, (1, 1), 0)
                img = cv2.dilate(img, None, iterations=2)
                img = cv2.erode(img, None, iterations=2)
                img = cv2.dilate(img, None, iterations=2)

                # Using haar cascade
                hands = handCascade.detectMultiScale(
                    img,
                    scaleFactor=1.1,
                    minNeighbors=5,
                    minSize=(50, 50),
                    flags=cv2.CASCADE_SCALE_IMAGE
                )
                for (x, y, w, h) in hands:
                    cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
                # End use haar cascade
                hand = segment(img.copy())
                if hand is not None:
                    cv2.drawContours(frame, hand, -1, (0, 255, 0), 3)
                    cv2.imshow("Can",img)
                    #--------------------------------------------------------------
                    # To count the number of fingers in the segmented hand region
                    #--------------------------------------------------------------
                    # fingers = count(img, hand)

                    # find the convex hull of the segmented hand region
                    chull = cv2.convexHull(hand)
                    # print ("after convexHull, there are " , str(len(chull)) , " points")
                    length = len(chull)
                    M = cv2.moments(hand)
                    if length > 5:
                    # print("there")
                        if M['m00'] != 0:
                            cx = int(M["m10"] / M["m00"])#找出中心的x座標
                            cy = int(M["m01"] / M["m00"])#找出中心的y座標
                        for i in range(length):
                            cv2.line(frame, tuple(chull[i][0]), (cx, cy), (0, 0, 255), 2)#從凸包輪廓連線到中心輪廓
                            cv2.line(frame, tuple(chull[i][0]), tuple(chull[(i+1)%length][0]), (0,0,255), 2)#從凸包輪廓連線到中心輪廓
                            cv2.circle(frame, (cx, cy), 10, (255, 0, 0), -1)#依照中心座標畫出圓點
                    # find the most extreme points in the convex hull
                    extreme_top    = tuple(chull[chull[:, :, 1].argmin()][0])
                    extreme_bottom = tuple(chull[chull[:, :, 1].argmax()][0])
                    extreme_left   = tuple(chull[chull[:, :, 0].argmin()][0])
                    extreme_right  = tuple(chull[chull[:, :, 0].argmax()][0])

                    # find the center of the palm
                    cX = int((extreme_left[0] + extreme_right[0]) / 2)
                    cY = int((extreme_top[1] + extreme_bottom[1]) / 2)

                    # find the maximum euclidean distance between the center of the palm
                    # and the most extreme points of the convex hull
                    distance = pairwise.euclidean_distances([(cX, cY)], Y=[extreme_left, extreme_right, extreme_top, extreme_bottom])[0]
                    maximum_distance = distance[distance.argmax()]

                    # calculate the radius of the circle with 80% of the max euclidean distance obtained
                    radius = int(0.8 * maximum_distance)

                    # find the circumference of the circle
                    circumference = (2 * np.pi * radius)

                    # take out the circular region of interest which has 
                    # the palm and the fingers
                    circular_roi = np.zeros(img.shape[:2], dtype="uint8")
                    
                    # draw the circular ROI
                    cv2.circle(circular_roi, (cX, cY), radius, 255, 1)

                    # take bit-wise AND between thresholded hand using the circular ROI as the mask
                    # which gives the cuts obtained using mask on the thresholded hand image
                    circular_roi = cv2.bitwise_and(img, img, mask=circular_roi)

                    # compute the contours in the circular ROI
                    ( cnts, _) = cv2.findContours(circular_roi.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

                    # initalize the finger count
                    count = 0

                    # loop through the contours found
                    for c in cnts:
                        # compute the bounding box of the contour
                        (x, y, w, h) = cv2.boundingRect(c)

                        # increment the count of fingers only if -
                        # 1. The contour region is not the wrist (bottom area)
                        # 2. The number of points along the contour does not exceed
                        #     25% of the circumference of the circular ROI
                        if ((cY + (cY * 0.25)) > (y + h)) and ((circumference * 0.25) > c.shape[0]):
                            count += 1
                    fingers = count

                    #--------------------------------------------------------------

                    cv2.putText(frame, str(fingers), (70, 45), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)
                cv2.imshow("Frame", frame)
                # cv2.putText(frame, str(fingers), (70, 45), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)

            
                # cv2.imshow("BG", bg_gray) 
                # cv2.imshow("difference", difference_mask.astype("uint8"))
                

                

            num_frames += 1
            keypress = cv2.waitKey(1) & 0xFF
            if keypress == ord("q"):
                break
        except Exception as e:
            print("[ERROR] ", e)
            time.sleep(0.5)
cap.release()
cv2.destroyAllWindows()