#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include "bmp.h"
#include <math.h>

#define BMP_FILEHEADER_SIZE 14
#define BMP_INFOHEADER_SIZE 40

BMP* readBMP(FILE* f);
GLBMP* readGLBMP(FILE* f);
void showBMPData(GLBMP* glbmp);
//int DCT(int matrix[DCT_matrix_size][DCT_matrix_size]);
void dimesionConert(unsigned char* data);
//int DCT_transform(unsigned char matrix[DCT_matrix_size][DCT_matrix_size]);
int DCT_transform(unsigned char *data,float *dctBuffer);
void DCT_process(unsigned char* data,int size,float *dctBuffer);
int IDCT_transform(unsigned char *data,float *dctBuffer);
void IDCT_process(unsigned char* data,int size,float *dctBuffer);
int DCT(unsigned char in[DCT_matrix_size][DCT_matrix_size]);
int IDCT(float in[DCT_matrix_size][DCT_matrix_size]);
BMP *bmp;
unsigned char bmp_data [262144][3]={0};

int main(void){
    
    // DCT(in);
    // printf("next\n");
    //IDCT(out);

    GLBMP *glbmp;
    GLBMP *glbmp2;
    //float dctBuffer[262144]={0};
    float *dctBuffer = (float*)malloc(262144*sizeof(float));
    printf("dctbuffer size : %d\n",sizeof(dctBuffer));
    FILE* f = fopen("../lena512.bmp", "rb");
    if( NULL == f ){

        printf( "open failure" );        
        return 1;

    }
    else{
        printf("open success !\n");

        // read bmp file
        glbmp=readGLBMP(f);
        fclose(f);
        // printf("%x\n",*(glbmp->glbmpData+1));
        DCT_process(glbmp->glbmpData, 512,dctBuffer);
        //IDCT_process(glbmp->glbmpData,16,dctBuffer);
        // // //write struct bmp to new file(grey level)
        FILE * newf2;
        newf2 = fopen ("lena_DCT.bmp", "wb");
        if(NULL==newf2){
            printf( "open failure" );        
            return 1;
        }
        else{
            printf("open success !\n");
            fwrite(glbmp,1,263222,newf2);   
            printf("a = : \n");
            fclose(newf2);
        }
        // //開啟IDCT
        // FILE* newf3= fopen("./lena_DCT.bmp", "rb");
        // if( NULL == newf3 ){
        //     printf( "open failure" );        
        // return 1;
        // }
        // else{
        //     printf("open IDCT success !\n");
        //     glbmp2=readGLBMP(newf3);
        //     fclose(newf3);
        //     IDCT_process(glbmp2->glbmpData, 16);

        //     FILE * newf4;
        //     newf4 = fopen ("lena_IDCT.bmp", "wb");
        //     int a=fwrite(glbmp2,1,263222,newf4);   
        //     fclose(newf4);
        // }
        
    }
    free(glbmp);
    free(glbmp2);
    free(dctBuffer);
    
    return 0;
}

BMP* readBMP(FILE* f){
    FILEHEADER fileheader;
    INFOHEADER infoheader;

    // Extract file header
    fread(&fileheader, sizeof(unsigned char),14, f);
    // Extract information header

    fread(&infoheader, sizeof(unsigned char),40, f);

    //Extract bmp data
    BMP *bmp1=(BMP *)malloc(sizeof(BMP)+3*infoheader.height*infoheader.width);
    fread((bmp1->bmpData),3,262144, f);

    // assign headers and bmp data to struct bmp
    bmp1->FILEHEADER = fileheader;
    bmp1->INFOHEADER = infoheader;
    printf("ok\n");
    return bmp1;
}
GLBMP* readGLBMP(FILE* f){
    
    GLBMP *glbmp0=(GLBMP *)malloc(263222);
    fread(glbmp0,1,263222, f);
    glbmp0->glbmpData = (&glbmp0->palette[1021])+1;
    // showBMPData(glbmp0);
    printf("%d\n",glbmp0->glbmpData[3]);
    printf("ok\n");

    return glbmp0;
}

void showBMPData(GLBMP* glbmp0){
    //TODO Little Endian 轉換
    printf("type :%x\n",glbmp0->FILEHEADER.type);
    printf("size :%d\n",glbmp0->FILEHEADER.size);
    printf("reserved1 :%d\n",glbmp0->FILEHEADER.reserved1);
    printf("offset :%d\n",(glbmp0->FILEHEADER.offset));
    printf("bmpHeaderSize :%d\n",glbmp0->INFOHEADER.bmpHeaderSize);
    printf("width :%d\n",glbmp0->INFOHEADER.width);
    printf("height :%d\n",glbmp0->INFOHEADER.height);
    printf("planes :%d\n",glbmp0->INFOHEADER.planes);
    printf("bitPerPixel :%d\n",glbmp0->INFOHEADER.bitPerPixel);
    printf("compression :%d\n",glbmp0->INFOHEADER.compression);
    printf("bmpDataSize :%d\n",glbmp0->INFOHEADER.bmpDataSize);
    printf("H_resolution :%d\n",glbmp0->INFOHEADER.H_resolution);
    printf("V_resolution :%d\n",glbmp0->INFOHEADER.V_resolution);
    printf("used_color :%d\n",glbmp0->INFOHEADER.used_color);
    printf("impot_color :%d\n",glbmp0->INFOHEADER.impot_color);
    for(int i=0;i<32;i++){
        for(int j=0;j<32;j++){
            printf("%d  ",glbmp0->palette[i*32+j]);
        }
        printf("\n");
    }
}
//int DCT_transform(unsigned char matrix[DCT_matrix_size][DCT_matrix_size]){
int DCT_transform(unsigned char *data,float *dctBuffer){
    int u, v, x, y; 
    // dct will store the discrete cosine transform 
    float dct[DCT_matrix_size][DCT_matrix_size]; 
    float beforedct[DCT_matrix_size][DCT_matrix_size]; 
    float cu, cv, dct1, sum; 
    //  printf("GGG1\n");
    // for(int i=0;i<8;i++){
    //     for(int j=0;j<8;j++){
    //         printf("%5.2x  ",*(data+i*8+j));
    //     }
    //     printf("\n");
    // }
    for (u = 0; u < DCT_matrix_size; u++) { 
        for (v = 0; v < DCT_matrix_size; v++) { 

            if (u == 0) 
                cu = 1 / sqrt(2); 
            else
                cu = 1; 
            if (v == 0) 
                cv = 1 / sqrt(2); 
            else
                cv = 1; 

            sum = 0; 
            for (x = 0; x < DCT_matrix_size; x++) { 
                for (y = 0; y < DCT_matrix_size; y++) { 
                    //
                    beforedct[x][y]=*(data+x*8+y);
                    //
                    sum = sum + beforedct[x][y] *  
                           cos((2 * x + 1) * u * pi / (2 * DCT_matrix_size)) *  
                           cos((2 * y + 1) * v * pi / (2 * DCT_matrix_size)); 
                } 
            } 
            dct[u][v] = cu * cv * sum*0.25;
            if(dct[u][v]>255){
                dct[u][v]=255;
            } 
            else if(dct[u][v]<0){
                dct[u][v]=0;
            }
            //
            // *(dctBuffer+1*sizeof(float))=dct[0][0];
            // 寫回圖片
            *(data+u*8+v)=(uint8_t)abs(dct[u][v]);
            //*(dctBuffer+u*32+v*4)=dct[u][v];
        } 
    }
    // printf("In teansform\n\n");
    // for(int i=0;i<8;i++){
    //     //printf("i=%d  ",i);
    //     for(int j=0;j<8;j++){
            
    //         printf("%5.2f  ",dct[i][j]);
    //         //*(dctBuffer+i*32+j*4)=dct[i][j];
    //     }
    //     printf("\n");
    // }
   
    //printf("In proccess\n\n");
}
void DCT_process(unsigned char* data,int size,float *dctBuffer){
    uint8_t F[8][8]={0};
    float *buffer=(float *)malloc(64*sizeof(float));

    // 轉成二維，並切割
    for(int k=0;k<size;k=k+DCT_matrix_size){
        for(int l=0;l<size;l=l+DCT_matrix_size){
            //printf("----- %d  %d -----\n",k,l);
            for(int i=0;i<DCT_matrix_size;i++){
                for(int j=0;j<DCT_matrix_size;j++){
                    F[i][j]=*(data+512*k+l+512*i+j);
                    
                }
            }

            // 對每個8*8進行dct
            DCT_transform(&F[0][0],buffer);
            
            // for(int i=0;i<8;i++){
            //     //printf("i=%d  ",i);
            //     for(int j=0;j<8;j++){
            //         printf("%5.2f  ",*(buffer+i*32+j*4));
            //         // 將DCT轉換後的資料存到一buffer，buffer內排列方式如原圖
            //         // *(dctBuffer+512*sizeof(float)*k+sizeof(float)*l+512*sizeof(float)*i+sizeof(float)*j)=*(buffer+8*sizeof(float)*i+sizeof(float)*j);
            //     }
            //     printf("\n");
            // }   

            //寫回圖片
            for(int i=0;i<8;i++){
                for(int j=0;j<8;j++){
                    // *(dctBuffer+512*k*sizeof(char)+l*sizeof(char)+512*i*sizeof(char)+j*sizeof(char))=*(buffer+i*8*sizeof(char)+j*sizeof(char));
                    //*(data+512*k*sizeof(char)+l*sizeof(char)+512*i*sizeof(char)+j*sizeof(char))=F[i][j];
                    *(data+512*k+l+512*i+j)=F[i][j];
                    //*(dctBuffer+512*sizeof(float)*k+sizeof(float)*l+512*sizeof(float)*i+sizeof(float)*j)=*(buffer+8*sizeof(float)*i+sizeof(float)*j);
                }
            }
            //free(buffer);
            
        }
    }
    
}
int IDCT_transform(unsigned char *data,float *dctBuffer){
    // dct will store the discrete cosine transform 
    float dct[DCT_matrix_size][DCT_matrix_size]; 
    float afterdct[DCT_matrix_size][DCT_matrix_size]; 
    float ci, cj, dct1, sum; 

    
    float cu, cv; 

	for (int x = 0; x < DCT_matrix_size; x++) { 
		for (int y = 0; y < DCT_matrix_size; y++) { 
		    
            sum = 0; 
			for (int u = 0; u < DCT_matrix_size; u++) { 
				for (int v = 0; v < DCT_matrix_size; v++) { 
				    
        			if (u == 0) 
        				cu = 1 / sqrt(2); 
        			else
        				cu =1; 
        			if (v == 0) 
        				cv = 1 / sqrt(2); 
        			else
        				cv =1; 

                    // afterdct[u][v]=*(data+8*u+v);
        			afterdct[u][v]=*(dctBuffer+4*8*u+4*v);
        			sum = sum + afterdct[u][v] * 
        				cos((2 * x + 1) * u * pi / (2 * DCT_matrix_size)) * 
        				cos((2 * y + 1) * v * pi / (2 * DCT_matrix_size))*cu*cv; 
				}
				dct[x][y] = sum*0.25; 
                *(data+x*8+y)=(uint8_t)dct[x][y];
			} 
            
		} 
	}
    printf("In IDCT teansform\n\n");
    for(int i=0;i<8;i++){
        printf("i=%d  ",i);
        for(int j=0;j<8;j++){
            
            printf("%5.2f  ",dct[i][j]);
            //*(dctBuffer+i*32+j*4)=dct[i][j];
        }
        printf("\n");
    }
   
    printf("In IDCT proccess\n\n"); 
}
void IDCT_process(unsigned char* data,int size,float *dctBuffer){
    unsigned char F[8][8]={0};
    float *buffer=(float *)malloc(64*sizeof(float));
    // 轉成二維
    printf("hi\n");
    for(int k=0;k<size;k=k+8){
        for(int l=0;l<size;l=l+8){
            //printf("----- %d  %d -----\n",k,l);
            for(int i=0;i<8;i++){
                for(int j=0;j<8;j++){
                    F[i][j]=*(data+512*k+l+512*i+j);
                }
            }

            //進行dct
            IDCT_transform(&F[0][0],buffer);

            //寫回圖片
            // for(int i=0;i<8;i++){
            //     for(int j=0;j<8;j++){
            //         *(data+512*k+l+512*i+j)=F[i][j];
            //     }
            // }
        }
    }
    
}
int DCT(unsigned char in[DCT_matrix_size][DCT_matrix_size]){
    int i, j, k, l; 
    // dct will store the discrete cosine transform 
    float dct[DCT_matrix_size][DCT_matrix_size]; 
    float beforedct[DCT_matrix_size][DCT_matrix_size]; 
    float ci, cj, dct1, sum; 

    for(int i=0;i<DCT_matrix_size;i++){
        for(int j=0;j<DCT_matrix_size;j++){
            printf("%3d  ",in[i][j]);
        }
        printf("\n");
    }


    for (i = 0; i < DCT_matrix_size; i++) { 
        for (j = 0; j < DCT_matrix_size; j++) { 
  

            if (i == 0) 
                ci = 1 / sqrt(2); 
            else
                ci = 1; 
            if (j == 0) 
                cj = 1 / sqrt(2); 
            else
                cj = 1; 
  
            sum = 0; 
            for (k = 0; k < DCT_matrix_size; k++) { 
                for (l = 0; l < DCT_matrix_size; l++) { 
                    //
                    //beforedct[k][l]=*(data+k*8+l);
                    //
                    dct1 = in[k][l] *  
                           cos((2 * k + 1) * i * pi / (2 * DCT_matrix_size)) *  
                           cos((2 * l + 1) * j * pi / (2 * DCT_matrix_size)); 
                    sum = sum + dct1; 
                    // sum = sum + beforedct[k][l] *  
                    //         cos((2 * k + 1) * i * pi / (2 * DCT_matrix_size)) *  
                    //         cos((2 * l + 1) * j * pi / (2 * DCT_matrix_size)); 
                } 
            } 
            dct[i][j] = ci * cj * sum*0.25;
        } 
    }

    for(int i=0;i<DCT_matrix_size;i++){
        for(int j=0;j<DCT_matrix_size;j++){
            printf("%5.3f    ",dct[i][j]);
        }
        printf("\n");
    }
    //IDCT(dct);
}
int IDCT(float in[DCT_matrix_size][DCT_matrix_size]){
    float dct[DCT_matrix_size][DCT_matrix_size]; 
    float afterdct[DCT_matrix_size][DCT_matrix_size]; 
    float ci, cj, sum; 

    
    float cu, cv; 

	for (int x = 0; x < DCT_matrix_size; x++) { 
		for (int y = 0; y < DCT_matrix_size; y++) { 
		    
            sum = 0; 
			for (int u = 0; u < DCT_matrix_size; u++) { 
				for (int v = 0; v < DCT_matrix_size; v++) { 
				    
        			if (u == 0) 
        				cu = 1 / sqrt(2); 
        			else
        				cu =1; 
        			if (v == 0) 
        				cv = 1 / sqrt(2); 
        			else
        				cv =1; 

                    //afterdct[u][v]=*(data+8*u+v);
        			//afterdct[u][v]=afterdct[u][v]*cu * cv;
        			sum = sum + in[u][v] * 
        				cos((2 * x + 1) * u * pi / (2 * DCT_matrix_size)) * 
        				cos((2 * y + 1) * v * pi / (2 * DCT_matrix_size))*cu*cv;  
				}
				 
                //*(data+x*8+y)=abs((int)dct[x][y]);
			} 
            dct[x][y] = sum*0.25;
		} 
	} 
    printf("ID\n");
    for(int i=0;i<DCT_matrix_size;i++){
        for(int j=0;j<DCT_matrix_size;j++){
            printf("%3f  ",dct[i][j]);
        }
        printf("\n");
    }
}