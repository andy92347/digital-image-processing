#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include "bmp.h"

#define BMP_FILEHEADER_SIZE 14
#define BMP_INFOHEADER_SIZE 40

BMP* readBMP(FILE* f);

BMP *bmp;
unsigned char bmp_data [262144][3]={0};

int main(void){
    //BMPImage *image = malloc(sizeof(*image));
    //BMP *bmp=malloc(sizeof(*bmp));

    FILE* f = fopen("../test.bmp", "rb");
    if( NULL == f ){

        printf( "open failure" );        
        return 1;

    }else{
        printf("open success !\n");

        // read bmp file
        bmp=readBMP(f);

        // change to grey level
        struct glbmp{
            struct fileheader FILEHEADER;
            struct infoheader INFOHEADER;
            unsigned char palette[1024];
            unsigned char glbmpData[bmp->INFOHEADER.height*bmp->INFOHEADER.width];
        }__attribute__((packed));;
        typedef struct glbmp GLBMP;

        GLBMP glbmp;
        glbmp.FILEHEADER = bmp->FILEHEADER;
        glbmp.INFOHEADER = bmp->INFOHEADER;
        glbmp.INFOHEADER.bitPerPixel = 8;
        glbmp.FILEHEADER.size = 263222;
        glbmp.FILEHEADER.offset = 1078;
        
        for(long int i=0,j=0;i<bmp->INFOHEADER.height*bmp->INFOHEADER.width;){
            glbmp.glbmpData[j] = ((float)bmp->bmpData[3*i]*0.114)+(float)bmp->bmpData[3*i+1]*0.587+(float)bmp->bmpData[3*i+2]*0.299;
            i++;
            j++;
        }

        for(int i=0;i<256;i++){
            glbmp.palette[4*i]   = i;
            glbmp.palette[4*i+1] = i;
            glbmp.palette[4*i+2] = i;
            glbmp.palette[4*i+3] = 0;
        }

        

        // write struct bmp to new file(grey level)
        FILE * newf2;
        newf2 = fopen ("lena_gl.bmp", "wb");
        fwrite(&glbmp,1,263222,newf2);   
        fclose(newf2);
        
        int8_t *temp;
        temp=(int8_t*)malloc(bmp->INFOHEADER.height*bmp->INFOHEADER.width);
        for(int i=0;i<256;i++){
            for(int j=0;j<256;j++){
                //temp[512*i+j]=glbmp.glbmpData[131072+512*i+j];
                temp[512*i+j]=glbmp.glbmpData[256+512*i+j];
            }
        }
        for(int i=0;i<256;i++){
            for(int j=0;j<256;j++){
                temp[256+512*i+j]=glbmp.glbmpData[131072+512*i+j];
            }
        }
        for(int i=0;i<256;i++){
            for(int j=0;j<256;j++){
                temp[131072+512*i+j]=glbmp.glbmpData[512*i+j];
            }
        }
        for(int i=0;i<256;i++){
            for(int j=0;j<256;j++){
                temp[131328+512*i+j]=glbmp.glbmpData[131328+512*i+j];
            }
        }
        for(long int i=0;i<bmp->INFOHEADER.height*bmp->INFOHEADER.width;i++){
            glbmp.glbmpData[i]=temp[i];                  
        }
        FILE * newf3;
        newf3 = fopen ("lena_mix.bmp", "wb");
        fwrite(&glbmp,1,263222,newf3);   
        fclose(newf3);
        //
        int count[256]={0};
        for(int i=0;i<bmp->INFOHEADER.height*bmp->INFOHEADER.width;i++){
            for(int j=0;j<256;j++){
                if(j==glbmp.glbmpData[i]){
                    count[j]++;
                }
            }
            //count[glbmp.glbmpData[i]]++;
        }
        
        FILE * newf4;
        newf4 = fopen ("lena_gl.csv", "w");
        //fwrite(&newf4,1,256,newf4);  
        for(int i=0;i<256;i++){ 
            fprintf(newf4,"%d,%d\n",i,count[i]);
        }
        fclose(newf4);

        // write struct bmp to new file
        FILE * newf;
        newf = fopen ("lena_orig.bmp", "wb");
        fwrite(bmp,1,sizeof(BMP)+3*bmp->INFOHEADER.height*bmp->INFOHEADER.width,newf);   
        fclose(newf);
        free(bmp);
        //free(temp);
    }
    
    fclose(f);
    return 0;
}

BMP* readBMP(FILE* f){
    FILEHEADER fileheader;
    INFOHEADER infoheader;

    // Extract file header
    fread(&fileheader, sizeof(unsigned char),14, f);
    // Extract information header

    fread(&infoheader, sizeof(unsigned char),40, f);

    //Extract bmp data
    BMP *bmp1=(BMP *)malloc(sizeof(BMP)+3*infoheader.height*infoheader.width);
    fread((bmp1->bmpData),3,262144, f);

    // assign headers and bmp data to struct bmp
    bmp1->FILEHEADER = fileheader;
    bmp1->INFOHEADER = infoheader;
    printf("ok\n");
    return bmp1;
}