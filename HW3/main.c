#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include "bmp.h"

#define BMP_FILEHEADER_SIZE 14
#define BMP_INFOHEADER_SIZE 40

#define threshold 120
#define struct_ele_size 5

BMP* readBMP(FILE* f);
void printData(BMP *bmp1);
BMP *bmp;
unsigned char bmp_data [262144][3]={0};

int main(void){
    //BMPImage *image = malloc(sizeof(*image));
    //BMP *bmp=malloc(sizeof(*bmp));

    // FILE* f = fopen("../hw3.bmp", "rb");
    FILE* f = fopen("../hw3.bmp", "rb");
    
    if( NULL == f ){

        printf( "open failure" );        
        return 1;

    }else{
        printf("open success !\n");

        // read bmp file
        bmp=readBMP(f);
        
        // Convert to gray level
        float buffer[375232] = {0};
        for(long int i=0,j=0;i<bmp->INFOHEADER.height*bmp->INFOHEADER.width;){
            buffer[j] = ((float)bmp->bmpData[3*i]*0.114)+(float)bmp->bmpData[3*i+1]*0.587+(float)bmp->bmpData[3*i+2]*0.299;
            bmp->bmpData[3*i]=buffer[j];
            bmp->bmpData[3*i+1]=buffer[j];
            bmp->bmpData[3*i+2]=buffer[j];
            i++;
            j++;
        }
        
        FILE * newf2;
        newf2 = fopen ("Gray_level.bmp", "wb");
        fwrite(&bmp->FILEHEADER,1,14,newf2);
        fwrite(&bmp->INFOHEADER,1,40,newf2);
        fwrite(&bmp->bmpData,1,1125696,newf2);       
        fclose(newf2);

        // Convert to binary image
        uint8_t tempFlag[375232]={0};
         printf("hi\n");
        for(int i=0;i<bmp->INFOHEADER.height;i++){
            for(int j=0;j<bmp->INFOHEADER.width;j++){
                if(bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]<threshold){
                bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]=0;
                bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3+1]=0;
                bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3+2]=0;
                }
                else if(bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]>=threshold){
                bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]=255;
                bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3+1]=255;
                bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3+2]=255;
                // tempFlag[(i*bmp->INFOHEADER.width+j)]=1;
                }
            }
        }
        
        FILE * newf3;
        newf3 = fopen ("Binary_image.bmp", "wb");
        fwrite(&bmp->FILEHEADER,1,14,newf3);
        fwrite(&bmp->INFOHEADER,1,40,newf3);
        fwrite(&bmp->bmpData,1,1125696,newf3);       
        fclose(newf3);

        // Catching edge
        for(int i=2;i<bmp->INFOHEADER.height-2;i++){
            for(int j=2;j<bmp->INFOHEADER.width-2;j++){
                if(bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)] || bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)] || bmp->bmpData[3*((i*bmp->INFOHEADER.width+j))]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)] || bmp->bmpData[3*((i*bmp->INFOHEADER.width+j))]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)]){
                    tempFlag[(i*bmp->INFOHEADER.width+j)]=1;
                }
            }
        }
        // Erosion
        for(int i=2;i<bmp->INFOHEADER.height-2;i++){
            for(int j=2;j<bmp->INFOHEADER.width-2;j++){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=0;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=0;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+2)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+2)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+2)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)+2]=0;
                    // 
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+657)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+657)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+657)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+655)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+655)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+655)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1312)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1312)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1312)+2]=0;
                    //
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-657)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-657)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-657)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-655)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-655)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-655)+2]=0;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)+1]=0;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)+2]=0;
                }
                
            }
        }
        FILE * erosion;
        erosion = fopen ("erosion.bmp", "wb");
        fwrite(&bmp->FILEHEADER,1,14,erosion);
        fwrite(&bmp->INFOHEADER,1,40,erosion);
        fwrite(&bmp->bmpData,1,1125696,erosion);       
        fclose(erosion);
        // catching edge
        for(int i=2;i<bmp->INFOHEADER.height-2;i++){
            for(int j=2;j<bmp->INFOHEADER.width-2;j++){
                tempFlag[(i*bmp->INFOHEADER.width+j)]=0;
                if(bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)] || bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)] || bmp->bmpData[3*((i*bmp->INFOHEADER.width+j))]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)] || bmp->bmpData[3*((i*bmp->INFOHEADER.width+j))]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)]){
                    tempFlag[(i*bmp->INFOHEADER.width+j)]=1;
                }
            }
        }
        // Dilation 
        for(int i=2;i<bmp->INFOHEADER.height-2;i++){
            for(int j=2;j<bmp->INFOHEADER.width-2;j++){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+2)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+2)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+2)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)+2]=255;
                    // 
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+657)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+657)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+657)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+655)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+655)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+655)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1312)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1312)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1312)+2]=255;
                    //
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-657)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-657)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-657)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-655)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-655)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-655)+2]=255;

                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)+2]=255;
                }
                
            }
        }
        FILE * dilation;
        dilation = fopen ("dilation.bmp", "wb");
        fwrite(&bmp->FILEHEADER,1,14,dilation);
        fwrite(&bmp->INFOHEADER,1,40,dilation);
        fwrite(&bmp->bmpData,1,1125696,dilation);       
        fclose(dilation);
        
        // catching edge
        for(int i=2;i<bmp->INFOHEADER.height-2;i++){
            for(int j=2;j<bmp->INFOHEADER.width-2;j++){
                tempFlag[(i*bmp->INFOHEADER.width+j)]=0;
                if(bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)] || bmp->bmpData[(i*bmp->INFOHEADER.width+j)*3]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)] || bmp->bmpData[3*((i*bmp->INFOHEADER.width+j))]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)] || bmp->bmpData[3*((i*bmp->INFOHEADER.width+j))]!=bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)]){
                    tempFlag[(i*bmp->INFOHEADER.width+j)]=1;
                }
            }
        }
        // region filling
        int k=bmp->INFOHEADER.height-450,l=450;
        // 第一象限
        int i,j;
        i=k,j=l;
        for(i=k;i<bmp->INFOHEADER.height-2;i++){
            j=l;
            if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
            }
            for(j=l;j<bmp->INFOHEADER.width-2;j++){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
                }
                else{
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)+2]=255;
                }
            }            
        }
        i=k,j=l;
        for(j=l;j<bmp->INFOHEADER.width-2;j++){
            i=k;
            if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
            }
            for(i=k;i<bmp->INFOHEADER.height-2;i++){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
                }
                else{
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)+2]=255;
                }
            }            
        }
        // 第三象限
        i=k,j=l; 
        for(i=k;i>2;i--){
            j=l;
            if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
            }
            for(j=l;j>2;j--){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
                }
                else{
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)+2]=255;
                }
            }            
        }
        i=k,j=l;
        for(j=l;j>2;j--){
            i=k;
            if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
            }
            for(i=k;i>2;i--){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
                }
                else{
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)+2]=255;
                }
            }            
        }
        // 第四象限
        i=k,j=l; 
        for(i=k;i<bmp->INFOHEADER.height-2;i++){
            j=l;
            if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
            }
            for(j=l;j>2;j--){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
                }
                else{
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-2)+2]=255;
                }
            }            
        }
        i=k,j=l;
        for(j=l;j>2;j--){
            i=k;
            if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
            }
            for(i=k;i<bmp->INFOHEADER.height-2;i++){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
                }
                else{
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+656)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1312)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1312)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1312)+2]=255;
                }
            }            
        }
        // 第二象限
        i=k,j=l;
        for(i=k;i>2;i--){
            j=l;
            if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
            }
            for(j=l;j<bmp->INFOHEADER.width-2;j++){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
                }
                else{
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+1)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+2)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+2)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)+2)+2]=255;
                }
            }            
        }
        i=k,j=l;
        for(j=l;j<bmp->INFOHEADER.width-2;j++){
            i=k;
            if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
            }
            for(i=k;i>2;i--){
                if(tempFlag[(i*bmp->INFOHEADER.width+j)]==1){
                    break;
                }
                else{
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+1]=255;
                    bmp->bmpData[3*(i*bmp->INFOHEADER.width+j)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-656)+2]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)+1]=255;
                    bmp->bmpData[3*((i*bmp->INFOHEADER.width+j)-1312)+2]=255;
                }
            }            
        }
        
        FILE * region_fill;
        region_fill = fopen ("region_fill.bmp", "wb");
        fwrite(&bmp->FILEHEADER,1,14,region_fill);
        fwrite(&bmp->INFOHEADER,1,40,region_fill);
        fwrite(&bmp->bmpData,1,1125696,region_fill);       
        fclose(region_fill);

    
        free(bmp);
        //free(temp);
    }
    
    fclose(f);
    return 0;
}

BMP* readBMP(FILE* f){
    FILEHEADER fileheader;
    INFOHEADER infoheader;

    // Extract file header
    fread(&fileheader, sizeof(unsigned char),14, f);
    // Extract information header

    fread(&infoheader, sizeof(unsigned char),40, f);

    //Extract bmp data
    BMP *bmp1=(BMP *)malloc(1125750);
    printf("%d\n",sizeof(BMP)+3*infoheader.height*infoheader.width);
    fread((bmp1->bmpData),3,375232, f);

    // assign headers and bmp data to struct bmp
    bmp1->FILEHEADER = fileheader;
    bmp1->INFOHEADER = infoheader;
    printf("ok\n");
    // printData(bmp1);
    return bmp1;
}
void printData(BMP *bmp1){
    printf("ID : %x\n",bmp1->FILEHEADER.type);
    printf("File Size : %d\n",bmp1->FILEHEADER.size);
    printf("Reserved : %d\n",bmp1->FILEHEADER.reserved1);
    printf("Offset : %d\n",bmp1->FILEHEADER.offset);
    printf("Width : %d\n",bmp1->INFOHEADER.width);
    printf("Height : %d\n",bmp1->INFOHEADER.height);
}