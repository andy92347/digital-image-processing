

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

typedef signed char             int8_t;   
typedef short int               int16_t;  
typedef int                     int32_t;  
 #define pixelNum 262144;

struct fileheader
{
    unsigned short int type;//2bytes
    unsigned int size;// File size in bytes,全部的檔案大小 4bytes
    unsigned int reserved1; // 保留欄位, 4byte
    unsigned int offset;// Offset to image data, 4bytes
}__attribute__((packed));;
typedef struct fileheader FILEHEADER;

struct infoheader
{
    unsigned int bmpHeaderSize;
    unsigned int width;
    unsigned int height; 
    unsigned short int planes;
    unsigned short int bitPerPixel;
    unsigned int compression;
    unsigned int bmpDataSize;
    unsigned int H_resolution;
    unsigned int V_resolution;
    unsigned int used_color;
    unsigned int impot_color;
}__attribute__((packed));;
typedef struct infoheader INFOHEADER;

//extern unsigned char bmpData [262144][3];

struct bmp
{
    struct fileheader FILEHEADER;
    struct infoheader INFOHEADER;
    //unsigned char palette[];
    unsigned char bmpData[] ;
}__attribute__((packed));;
typedef struct bmp BMP;

struct glbmp
{
    struct fileheader FILEHEADER;
    struct infoheader INFOHEADER;
    unsigned char palette[1024];
    unsigned char glbmpData[];
}__attribute__((packed));;
typedef struct glbmp GLBMP;
